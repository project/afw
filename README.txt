AFW: Available for work

This module provides a basic block that will let you tell others if you are
available for work. You can change the block title and the phrasing that
appears from the block configuration page. The default value is set to say
that you are accepting more work. I'm not sure if that's optimistic or
pessimistic, but you can change it whenever you'd like.

This was largely meant as a quick solution to a problem that would also help me learn the
changes to D7 block setup and how it was different from D6. At the same time, it felt like
something that would be re-used.

If you run your own freelancing practice, it's generally a good idea to let people know
on your site whether you're accepting new work; it's pretty much one of the
few things people will actually use your site for.

Hope you find it useful. I don't expect a lot of feature enhancements to this, but
you never know. If you have an idea, feel free to send it along.

Maintainers

- Trevor Twining (trevortwining)